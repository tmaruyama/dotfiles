if has('nvim')
let g:python_host_prog = '/Users/tmaruyama/.pyenv/shims/python'
let g:python3_host_prog ="/Users/tmaruyama/.pyenv/shims/python3"
endif

" dein.vim Initialization {{{
let s:dein_dir = expand('~/.cache/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

if &runtimepath !~# 'dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)
  call dein#load_toml('~/dotfiles/.vim/dein.toml', {'lazy': 0})
  call dein#load_toml('~/dotfiles/.vim/dein_lazy.toml', {'lazy': 1})
  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif
" }}}

" Setup tab page {{{
" Anywhere SID.
function! s:SID_PREFIX()
  return matchstr(expand('<sfile>'), '<SNR>\d\+_\zeSID_PREFIX$')
endfunction

function! s:my_tabline()  "{{{
  let s = ''
  for i in range(1, tabpagenr('$'))
    let bufnrs = tabpagebuflist(i)
    let bufnr = bufnrs[tabpagewinnr(i) - 1]
    let no = i  " display 0-origin tabpagenr.
    let mod = getbufvar(bufnr, '&modified') ? '!' : ' '
    let title = fnamemodify(bufname(bufnr), ':t')
    let title = '[' . title . ']'
    let s .= '%'.i.'T'
    let s .= '%#' . (i == tabpagenr() ? 'TabLineSel' : 'TabLine') . '#'
    let s .= no . ':' . title
    let s .= mod
    let s .= '%#TabLineFill# '
  endfor
  let s .= '%#TabLineFill#%T%=%#TabLine#'
  return s
endfunction "}}}
let &tabline = '%!'. s:SID_PREFIX() . 'my_tabline()'
set showtabline=2

nnoremap    [Tag]   <Nop>
nmap    t [Tag]

for n in range(1, 9)
  execute 'nnoremap <silent> [Tag]'.n  ':<C-u>tabnext'.n.'<CR>'
endfor

map <silent> [Tag]c :tablast <bar> tabnew<CR>
map <silent> [Tag]x :tabclose<CR>
map <silent> [Tag]n :tabnext<CR>
map <silent> [Tag]p :tabprevious<CR>
" }}}



" Encoding {{{
set encoding=utf8
set fileencodings=utf-8,iso-2022-jp,euc-jp,sjis
" }}}


" Key assignment {{{
let mapleader = "\<Space>"
nnoremap <Leader>o :NERDTree<CR>
nnoremap <Leader>w :w<CR>
nnoremap <Leader>t :terminal<CR>
nmap <Leader><Leader> V
tnoremap <silent> <ESC> <C-\><C-n>
" }}}


" Display {{{
set title
set ambiwidth=double
set laststatus=2

set number
set colorcolumn=80
set nowrap
set cursorline

set cindent
set shiftwidth=4
set softtabstop=4
set expandtab
set wildmenu
set wildmode=longest,full
" }}}


" Search {{{
set smartcase
set wrapscan
set incsearch
set hlsearch
" }}}


" Editing {{{
set hidden
set showmatch
set history=10000
set autoread
set backspace=indent,eol,start
" }}}

" Others {{{
set sh=zsh
set mouse=a
colors iceberg
syntax enable
" }}}
