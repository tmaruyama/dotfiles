# path
export PATH=/Users/tmaruyama/.pyenv/shims:$PATH
export PATH=/Users/tmaruyama/tools/bin:$PATH
export PATH=/usr/local/Cellar:$PATH
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# alias
alias mecab-ipadic='mecab -d /Users/tmaruyama/tools/mecab-0.996/lib/mecab/dic/ipadic'
alias mecab-unidic='mecab -d /Users/tmaruyama/tools/mecab-0.996/lib/mecab/dic/unidic-cwj-2.3.0'
alias mecab-unidic-neologd='mecab -d /Users/tmaruyama/tools/mecab-0.996/lib/mecab/dic/mecab-unidic-neologd'

# Variable
export XDG_CONFIG_HOME=~/.config

# Autoloadings
autoload -Uz add-zsh-hook
autoload -Uz compinit && compinit -u
autoload -Uz url-quote-magic
autoload -Uz vcs_info

# ZLE settings
zle -N self-insert url-quote-magic

# General settings
setopt auto_list
setopt auto_menu
setopt auto_pushd
setopt extended_history
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt inc_append_history
setopt interactive_comments
setopt no_beep
setopt no_hist_beep
setopt no_list_beep
setopt magic_equal_subst
setopt notify
setopt print_eight_bit
setopt print_exit_value
setopt prompt_subst
setopt pushd_ignore_dups
setopt rm_star_wait
setopt share_history
setopt transient_rprompt

# Exports
export CLICOLOR=true
export LSCOLORS='exfxcxdxbxGxDxabagacad'
export EDITOR=vim
export HISTFILE=~/.zhistory
export HISTSIZE=1000
export SAVEHIST=1000000
export LANG=ja_JP.UTF-8

# Key bindings
bindkey -e
